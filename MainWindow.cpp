#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "RenderEngine.h"
#include "ColladaImport.h"
#include "MyGeometry.h"
#include "Vector3f.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    eng = NULL;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete eng;
}

void MainWindow::init()
{
    /* create a new render engine and put it into the frame */
    eng = new RenderEngine(this);
    eng->init();
    this->ui->frameForGL->layout()->addWidget(eng);
}

void MainWindow::on_zoomInButton_clicked()
{
    if(eng != NULL)
    {
        eng->dispCameraR(-1.0f);
    }
}

void MainWindow::on_zoomOutButton_clicked()
{
    if(eng != NULL)
    {
        eng->dispCameraR(1.0f);
    }
}

void MainWindow::on_upButton_clicked()
{
    if (eng != NULL)
    {
        eng->dispCameraXZ(5.0f);
    }
}

void MainWindow::on_downButton_clicked()
{
    if (eng != NULL)
    {
        eng->dispCameraXZ(-5.0f);
    }
}

void MainWindow::on_leftButton_clicked()
{
    if (eng != NULL)
    {
        eng->dispCameraY(5.0f);
    }
}

void MainWindow::on_rightButton_clicked()
{
    if (eng != NULL)
    {
        eng->dispCameraY(-5.0f);
    }
}

void MainWindow::on_loadFileButton_clicked()
{
    Vector3f scale(0.0025f,0.0025f,0.0025f);
    loadColladaModelFromFile(scale);
}

void MainWindow::loadColladaModelFromFile(Vector3f scale)
{
    QString path = QFileDialog::getOpenFileName(this, tr("Open Model"), "", tr("DAE files (*.dae);;All Files (*)"));
    while (!eng->getDrawList().empty()) //while stage isn't empty
    {
        eng->remove();
    }

    ColladaImport *imp = new ColladaImport();
    imp->loadColladaFile(path.toAscii().data());

    vector<MyGeometry*> geom = imp->getMyGeom();
    for (unsigned int i = 0; i < geom.size(); i++)
    {
        geom[i]->setScale(scale.getX(), scale.getY(), scale.getZ());

        if(geom[i] != NULL)
        {
            eng->add(geom[i]);
        }
        else
        {
            QMessageBox messageBox;
            QString message("Could not loade model from file: ");
            message +=path;
            messageBox.critical(this,"Model Load Error",message);
            messageBox.setFixedSize(500,200);
        }
    }

    delete imp;
}
