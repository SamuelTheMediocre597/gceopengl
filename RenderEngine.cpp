#include "RenderEngine.h"
#include "Object3D.h"

RenderEngine::RenderEngine(QWidget *parent) : MyQtGLWindow(parent)
{
    this->drawAxis = true;
}

RenderEngine::~RenderEngine()
{
    //Dealocate draw list

    Object3D* temp;

    //iterartor for vector
    std::vector<Object3D*>::iterator it = drawList.begin();

   while(drawList.empty() == false)
    {
        temp = (*it);
        it = drawList.erase(it);
        delete temp;
    }
}

void RenderEngine::init()
{
    MyQtGLWindow::init();
}

void RenderEngine::draw()
{
    if(this->drawAxis == true)
        this->drawPrincipleAxis();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_RESCALE_NORMAL);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    std::vector<Object3D*>::iterator it;

    for(it = drawList.begin(); it != drawList.end(); ++it)
    {
        (*it)->draw(); //.at(i) returns a poitner
                                //to the object at i in the vector
    }

    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glDisable(GL_RESCALE_NORMAL);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
}

void RenderEngine::setDrawList(vector<Object3D*> list)
{
    drawList = list;
}

vector<Object3D*> RenderEngine::getDrawList(void)
{
    return drawList;
}

void RenderEngine::add(Object3D *obj)
{
    drawList.push_back(obj);
}

void RenderEngine::remove()
{
    drawList.pop_back();
}

void RenderEngine::axisOn()
{
    this->drawAxis = true;
}

void RenderEngine::axisOff()
{
    this->drawAxis = false;
}



