#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class RenderEngine;
class Vector3f;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init();

    void loadColladaModelFromFile(Vector3f scale);

private slots:
    void on_zoomInButton_clicked();
    void on_zoomOutButton_clicked();

    void on_upButton_clicked();
    void on_downButton_clicked();
    void on_leftButton_clicked();
    void on_rightButton_clicked();

    void on_loadFileButton_clicked();

private:
    Ui::MainWindow *ui;
    RenderEngine* eng;
};

#endif // MAINWINDOW_H
