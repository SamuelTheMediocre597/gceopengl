#include <QtCore>
#include <QApplication>

#include "MainWindow.h"
#include "Vector3f.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	
    MainWindow window;
    window.init();
    window.resize(800,800);
    window.setWindowTitle("COLLADA File Loader");

    Vector3f scale(0.0025f,0.0025f,0.0025f);

    window.loadColladaModelFromFile(scale);

    window.show();

    return a.exec();
}
