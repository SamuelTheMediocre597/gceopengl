#include "MyGeometry.h"
#include "MyPolygon.h"
#include "Vector3f.h"

#include <iostream>
using std::cout;
using std::endl;

//FOR GL TYPES
#include <qgl.h>
#include <QtGui>

MyGeometry::MyGeometry(void)
{
	vertexCount = 0;
	normalCount = 0;
    scale.setX(1.0f);
    scale.setY(1.0f);
    scale.setZ(1.0f);
}

MyGeometry::~MyGeometry(void)
{
	
	Vector3f* temp; 
	MyPolygon* tempPoly;
	while(!verts.empty())
	{
		temp = verts.back();
		verts.pop_back();
        delete temp;
	}

	while(!norms.empty())
	{
		temp = norms.back();
		norms.pop_back();
        delete temp;
	}

	while(!polys.empty())
	{
		tempPoly = polys.back();
		polys.pop_back();
        delete tempPoly;
	}
}

int MyGeometry::getVertexCount()
{
	return vertexCount;
}

int MyGeometry::getNormalCount()
{
	return normalCount;
}

void MyGeometry::addVertex(Vector3f* v)
{
	verts.push_back(v);
    vertexCount++;
}

void MyGeometry::addNormal(Vector3f* v)
{
	norms.push_back(v);
    normalCount++;
}

void MyGeometry::removeVertex()
{
	verts.pop_back();
    vertexCount--;
}

void MyGeometry::removeNormal()
{
	norms.pop_back();
    normalCount--;
}

void MyGeometry::addVertNormPair(Vector3f* v, Vector3f* n)
{
	verts.push_back(v);
    vertexCount++;

	norms.push_back(n);
    normalCount++;
}

void MyGeometry::removeVertexNormPair()
{
	verts.pop_back();
    vertexCount--;

	norms.pop_back();
    normalCount--;
}

Vector3f* MyGeometry::getVertexN(int i)
{
	return verts.at(i);
}

Vector3f* MyGeometry::getNormalN(int i)
{
	return norms.at(i);
}

MyPolygon* MyGeometry::getPolygonN(int i)
{
	return polys.at(i);
}

void MyGeometry::addPolygon(MyPolygon *p)
{
	polys.push_back(p);
}

void MyGeometry::draw()
{
	int i,j;
	int numberOfFaces = polys.size();
	MyPolygon *p; //local pointer to face
	Vector3f *vert;
	Vector3f *norm;
	int normIndex;
	int vertIndex;

	int mode;

	//work out how may verts per face
	switch(polys.front()->getIndexCount()) 
	{
		case 3:
			mode = GL_TRIANGLES;
			break;
		case 4:
			mode = GL_QUADS;
			break;
		default:
			mode = GL_POLYGON;
			break;
	}

    glPushMatrix();
        glScalef(scale.getX(), scale.getY(), scale.getZ());

        glBegin(mode);

            for(i=0;i<numberOfFaces; i++)
            {
                //Draw each face - i.e draw each point
                p = polys.at(i);

                int indexCount = p->getIndexCount();
                for(j=0;j<indexCount;j++)
                {
                    vertIndex = p->getVertIndexN(j);   
                    vert = verts.at(vertIndex);

                    if (!norms.empty())
                    {
                        normIndex = p->getNormIndexN(j);
                        norm = norms.at(normIndex);
                        glNormal3f(norm->getX(),norm->getY(),norm->getZ());
                    }
                    glVertex3f(vert->getX(),vert->getY(),vert->getZ());
                }
            }

        glEnd();
    glPopMatrix();

    glPushMatrix();
        glScalef(scale.getX(), scale.getY(), scale.getZ());
        glColor3f(0.0, 1.0, 0.0);

        glBegin(GL_LINES);
        for(i=0;i<numberOfFaces; i++)
        {
            //Draw each face - i.e draw each point
            p = polys.at(i);
            int indexCount = p->getIndexCount();

            for(j=0;j<indexCount;j++)
            {
                if (!norms.empty())
                {
                    vertIndex = p->getVertIndexN(j);
                    vert = verts.at(vertIndex);

                    normIndex = p->getNormIndexN(j);
                    norm = norms.at(normIndex);

                    glVertex3f(vert->getX(),vert->getY(),vert->getZ());
                    glVertex3f(vert->getX() + (norm->getX() * 5),vert->getY() + (norm->getY() * 5),vert->getZ() + (norm->getZ() * 5));
                }
            }
        }
        glEnd();

    glPopMatrix();
}

void MyGeometry::setScale(float x, float y, float z)
{
    scale.setX(x);
    scale.setY(y);
    scale.setZ(z);
}

void MyGeometry::printVerts()
{
    Vector3f* temp;
    cout << "------------ Start Vertex Print ------------\n" << endl;
    for(unsigned int i =0; i<verts.size(); i++)
    {
        temp = verts.at(i);
        cout << "Vertex " << i << " :- {" << temp->getX() << ", " << temp->getY() << ", " <<  temp->getZ() << "}" << endl;
    }
    cout << "------------ End Vertex Print --------------\n" << endl;
}

void MyGeometry::printNormals()
{
    Vector3f* temp;
    cout << "------------ Start Normals Print ------------\n" << endl;
    for(unsigned int i =0; i<norms.size(); i++)
    {
        temp = norms.at(i);
        cout << "Normal " << i << " :- {" << temp->getX() << ", " << temp->getY() << ", " <<  temp->getZ() << "}" << endl;
    }
    cout << "------------ End Normals Print --------------\n" << endl;
}

void MyGeometry::printPolyIndexes()
{
    MyPolygon *temp;
    cout << "----------- Start Polygon Index Print ------------\n" << endl;
    for(unsigned int i =0; i<polys.size(); i++)
    {
        temp = polys.at(i);
        cout << "======= Start Normals and Vertexs for Polygon " << i << " =======" << endl;
        temp->printVertIndex();
        if (!norms.empty())
        {
            temp->printNormIndex();
        }
        cout << "======= End Normals and Vertexs for Polygon " << i << " =======" << endl;
    }
}
