
#include "ColladaImport.h"

#include <string>		//collada uses c++ strings
#include <iostream>
using namespace std;	//this avoids putting std:: before each string call. 

#include "MyGeometry.h"
#include "MyPolygon.h"
#include "Vector3f.h"


ColladaImport::ColladaImport(void)
{
    // Instantiate the reference implementation
    dae = new DAE(NULL,NULL,"1.4.1");

    //Set object to null
    local = NULL;

	//set flag - this will be used later to ensure that the file is sucessfully loaded
	//throwing exceptions would be better here but are beyond the current scope. 
	fileLoadedFlag = false;
}

ColladaImport::~ColladaImport(void)
{
    delete(dae);
}

/*Currently hardcoded! */
void ColladaImport::loadColladaFile(char *URL)
{
    //load the COLLADA file

    /* Note - COLLADA uses version 1.35 of the boost filesystem library*/
    boost::filesystem::path p(URL);

    if(!boost::filesystem::exists(p))
    {
        cout << p.string() << " does not exist" << endl;
        fileLoadedFlag = false;
        return;
    }

    string path = cdom::nativePathToUri(boost::filesystem::system_complete(p).string());
    path = "file://"+path;
    cout << "File path=" << path << endl;
    root = dae->open(path);

    //if result is not an error code set the flag to say the file is loaded
    if(root == 0)
        fileLoadedFlag = false;
    else
        fileLoadedFlag = true;


    //print children
    printChildren("document root", root);
	
	//extract the geometry - or navigate it as the case may be
	extractMyGeometry();
}

void ColladaImport::extractMyGeometry()
{
	if(fileLoadedFlag)
	{
		//Navigate to the library of geometries
		daeElement* libGeom = root->getDescendant("library_geometries");

		//How many geometry elements are there?! 
		daeTArray< daeSmartRef<daeElement> >geometryElements =  libGeom->getChildren();
		int numberOfGeometries = (int)geometryElements.getCount();

       for (int i = 0; i < numberOfGeometries; i++)
       {
            // Get a properly typed pointer to the COLLADA domGeometry element
            domGeometry* geometryElement = daeSafeCast<domGeometry>(geometryElements[i]);

            // Get a pointer to the domGeometry's domMesh element
            domMesh *meshElement = geometryElement->getMesh();

            /*Decide how to process the mesh information, there are several possiblities
            This example is limited in that it only works with triangulated meshes */
            if(meshElement->getDescendant("triangles"))
            {
                // Instanciate the geometry we're going to create
                local = new MyGeometry();
                getTriangles(meshElement,local);
                list.push_back(local);
            }
            else
            {
                printf("Unsupported geometry encoding!\n");
                printf("Only triangles supported at present\n");
            }

            printf("End of geometry library - next object\n");
        }
    }
}

void ColladaImport::getTriangles(domMesh* meshElement, MyGeometry* local)
{
	int triangleCount;	//number of triangles
	int indexCount;		// total lenght of the <p> array
	
	//offset data so we start in the right place when processing 
	//these arrays
	int vOffset = -1;	//vertex offset in <p> array
    int nOffset = -1;
    int tOffset = -1;

	//string id's of source arrays - so we process the right one to the right place
	char* vertexArrayID = NULL; //must be mapped to <verticies><source>
    char* normalArrayID = NULL;
    char* textCoordArrayID = NULL;

	//Interrogate the dom, find out how may triangles we have
	//assume only one triangle library
	domTriangles *t = meshElement->getTriangles_array()[0]; 
	triangleCount = t->getCount();

	printf("There are %d triangles \n",triangleCount);
	
	/**************************************** 
	 * Process indicies (vertex and normal) * 
	 ****************************************/

	//Get the <p> array - interleaved indexes into source arrays (vert, norm, text coord)
	domListOfUInts P = t->getP()->getValue();
	indexCount = (int)P.getCount();

	printf("There are %d indexes \n",indexCount);

	//Get the meta data regarding the indexes
    getMetadata(t,&vOffset, &nOffset, &tOffset, &vertexArrayID, &normalArrayID, &textCoordArrayID);

	//need to step over as many elements as are present to move to the next one. 
	int pStep = (int)t->getInput_array().getCount();
	
	//Send the information to process indexes this will extract indexes for
	//the triangle norms and verts and create new polygons in the myGeometry 
	//object with those indexes. 
    processIndexes(local, P, indexCount, pStep, nOffset, vOffset, tOffset);

	/************************
	* Process source arrays *
	*************************/
	
	//Number of source arrays
	//This includes - the position array (referenced though the normal array!)
	//Cannot assume order, so must use 
    int sourceArrayCount = (int)meshElement->getSource_array().getCount();

	//store the name <verticies> array - not used
	char* verticiesID =(char*)meshElement->getVertices()->getId();
	//store the id of the source of the verticeis array
	//input object for vertex will give verticiesID as its source but
	//actual source is the source of the <verticies> so store this!

    //might create normalID array or something
	char* positionID = (char*)((meshElement->getVertices()->getInput_array().get(0))->getSource().getID());

    processVertexes(local, meshElement,sourceArrayCount, verticiesID, positionID,normalArrayID);

	//free memory, 

	free(vertexArrayID);
    free(normalArrayID);
    free(textCoordArrayID);

    //TODO clear other indexes.
}

void ColladaImport::getMetadata(domTriangles *t, int *vOffset, int *nOffset, int *tOffset, char** vertexArrayID, char** normalArrayID, char** textureArrayID)
{
	//Look at the semantics of the input arrays to decide how <p> is organised
	//and process it accordingly

	int strLen;
	for(unsigned int i =0; i < t->getInput_array().getCount(); i++)
	{
		domInputLocalOffsetRef d1 = t->getInput_array().get(i);
        printf("%s\n",d1->getElementName());

		//get input array semantic string
		char* str = (char*)d1->getSemantic();

		if(strcmp(str,"VERTEX")==0)
		{
			*vOffset = d1->getOffset(); //numerical offset of data in <p>
			strLen = strlen(d1->getSource().getID());
			*vertexArrayID = (char*)malloc(strLen*sizeof(char)+1);
			strcpy(*vertexArrayID,(char*)d1->getSource().getID()); //id of <source> array
            printf("Found atribute %s , value recoverd %d \n", str, *vOffset);
		}
		else
		{
			if(strcmp(str,"NORMAL")==0)
			{
                *nOffset = d1->getOffset();
                strLen = strlen(d1->getSource().getID());
                *normalArrayID = (char*)malloc(strLen*sizeof(char)+1);
                strcpy(*normalArrayID, (char*)d1->getSource().getID());
                printf("Found attribute %s, value recovered %d \n", str, *nOffset);
			}
			else
			{
                if(strcmp(str,"TEXCOORD")==0)
                {
                    *tOffset = d1->getOffset();
                    strLen = strlen(d1->getSource().getID());
                    *textureArrayID = (char*)malloc(strLen*sizeof(char)+1);
                    strcpy(*textureArrayID, (char*)d1->getSource().getID());
                    printf("Found attribute %s, value recovered %d \n", str, *tOffset);
                }
                else
                {
					//something else?!
					printf("Attribute found %s \n", str);
                }
			}
		}
	}
}

void ColladaImport::processIndexes(MyGeometry* geom, domListOfUInts P, int indexCount, int pStep, int nOffset, int vOffset, int tOffset)
{
	//Local arrays - to contain one index each / by number of entries
	//ie the step calculated above
	int* pVertIndex = (int*)malloc(((indexCount/pStep) * sizeof(int)));
    int* pNormIndex = (int*)malloc(((indexCount/pStep) * sizeof(int)));
    int* pTextIndex = (int*)malloc(((indexCount/pStep) * sizeof(int)));

	int i; //index into <p>
	int j; //index into internal arrays
	
	//read from single <p> array into two seperate arrays
	for(i=0,j =0; i<indexCount; i+=pStep, j++)
	{
		pVertIndex[j] = P[vOffset+i];
        if (nOffset > -1)
        {
            pNormIndex[j] = P[nOffset+i];

            if (tOffset > -1)
            {
                pTextIndex[j] = P[tOffset+i];
                printf("Vertex %d --- Normal %d --- Texture %d\n", pVertIndex[j], pNormIndex[j], pTextIndex[j]);
            }
            else
            {
                printf("Vertex %d --- Normal %d\n", pVertIndex[j], pNormIndex[j]);
            }
        }
        else
        {
            printf("Vertex %d\n", pVertIndex[j]);
        }
	}

	//ready from seperate arrays into a polygon
	for(int i=0; i<j; i+=3)
	{
		//create a new polygone object, set the number 
		//of indicies and read in the indexes
		//3 indexes per triangle 
		MyPolygon *p = new MyPolygon();
		p->setIndexCount(3);
		
		//assign indexes
		p->addVertIndex(pVertIndex[i+0]);
		p->addVertIndex(pVertIndex[i+1]);
		p->addVertIndex(pVertIndex[i+2]);

        if (nOffset > -1)
        {
            //assign normals - 1 normal per vertex
            p->addNormIndex(pNormIndex[i+0]);
            p->addNormIndex(pNormIndex[i+1]);
            p->addNormIndex(pNormIndex[i+2]);
        }

        if (tOffset > -1)
        {
            //assign textures
            p->addTextIndex(pTextIndex[i+0]);
            p->addTextIndex(pTextIndex[i+1]);
            p->addTextIndex(pTextIndex[i+2]);
        }

		//add the polygon to the end of the vector
        geom->addPolygon(p);
	}

	free(pVertIndex);
    free(pNormIndex);
    free(pTextIndex);

	//Debugging printout - takes a long time!
    geom->printPolyIndexes();
}

void ColladaImport::processVertexes(MyGeometry* geom, domMesh* meshElement, int sourceArrayCount, char* vertexArrayID, char* positionID, char* normalArrayID)
{
	//There may be a number of source arrays
	char* sourceID; //current source;
	unsigned int stride; //how many elements per vertex (3 or 4)
	domFloat_array *floatArray; //stores the source data
	unsigned int sourceCount;

	//For each source array check whether it maches the stored sorce ids for vertex
	//or normals then store it in the appropriate place within our 
	//MyGeometry object
	for(int i=0; i<sourceArrayCount; i++)
	{
		//get the ith source array
		domSource *source = meshElement->getSource_array()[i];
		//get its id
		sourceID = (char*)source->getID();

		if(source->getFloat_array()==NULL)
		{
			printf("Float array was NULL possible mall formed collada file\n");
		}
		else
		{
			//get the array of floats
			floatArray = source->getFloat_array(); 
			
			// Interrogate the common technique to get stride
			stride = (unsigned int)source->getTechnique_common()->getAccessor()->getStride();

			//set source count
			sourceCount = (unsigned int)floatArray->getCount();

			//if this is the vertex array
			if((strcmp(sourceID,vertexArrayID)==0)||(strcmp(sourceID,positionID)==0)) 
			{
				// Copy the vertices into my structure one-by-one 
				//(converts from COLLADA's doubles to floats)
				unsigned int i;
				for (i = 0; i < sourceCount; i+=stride) 
				{
					//Assumes x,y,z order - could interrogate accessor to check this
                    geom->addVertex(new Vector3f(floatArray->getValue()[i],
													floatArray->getValue()[i+1],
													floatArray->getValue()[i+2]));
				}

				//  debuggnig printout has a big performance impact
                geom->printVerts();
			}
			else 
			{
                //if source array is the normals array
                if (normalArrayID != NULL)
                {
                    if (strcmp(sourceID, normalArrayID) == 0)
                    {
                        unsigned int i;
                        for (i = 0; i < sourceCount; i += stride)
                        {
                            geom->addNormal(new Vector3f(floatArray->getValue()[i],
                                                          floatArray->getValue()[i+1],
                                                          floatArray->getValue()[i+2]));
                        }

                        geom->printNormals();
                    }
                    else
                    {
                        cout << "unrecognised array - NEXT" << endl;
                    }
                }
			}
		}
	}
}

/*Print children takes a daeElement - this is a superclass of most other element types 
and so we can pass many of the children to it */
void ColladaImport::printChildren(char* identifyingString, daeElement* root)
{
	printf("\nSTART - %s\n", identifyingString);
	/* the daeTArray is a type commonly used within the dom it provides a method
	for implementing parent child links i.e. each parent has a daeTArray of children
	the daeElementRef is a special reference type which keeps track of the number of 
	active poniters to the object, helping to avoid memory leaks. */

	daeTArray<daeElementRef> children = root->getChildren();
	for (size_t i = 0; i < children.getCount(); i++)
	{
		printf("child %d is named %s \n",i,children[i]->getElementName());
	}
	printf("END - %s\n", identifyingString);
}

/*gets the geometry function extracted from the dea */
/*
 * WARNING: This assumes object which takes the geom is
 * responsible for getting rid of the memory
 */
std::vector<MyGeometry*> ColladaImport::getMyGeom()
{
    if(fileLoadedFlag == true)
        return list;
    else
        return vector<MyGeometry*>();
}
