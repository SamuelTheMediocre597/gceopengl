/*
 * MyQtGLWindow.cpp
 *
 *  Created on: 16-May-2009
 *      Author: tigger
 */

#include "MyQtGLWindow.h"
#include <iostream>

MyQtGLWindow::MyQtGLWindow(QWidget* parent) : QGLWidget(parent)
{

}

void MyQtGLWindow::init()
{
    //set up view defaults
    vpWidth = 10.0f;
    vpHeight = 10.0f;
    nearPlane = 1.0f;
    farPlane = 1500.0f;

    // initialise camera position and direction
    cameraXZ = 0.0f;
    cameraY = 0.0f;
    cameraR = 5.0f;

    this->initalizeGL();
}

MyQtGLWindow::~MyQtGLWindow()
{
	//  Auto-generated destructor stub
}

void MyQtGLWindow::initalizeGL()
{
    /********************************************
     * Set rendering properties
     * *******************************************/
    glShadeModel(GL_SMOOTH);						// smooth shading model

	//Make sure window is RGBA
	QGLFormat fmt = this->format();
	fmt.setAlpha(true);
	fmt.setDoubleBuffer(true);
	fmt.setDepth(true);
	fmt.setRgba(true);
	this->setFormat(fmt);

    bool fmtCheck = this->format().rgba();

    if(fmtCheck != true)
    {
        QMessageBox messageBox;
        messageBox.critical(this,"Warning","RGBA format was not set!");
        messageBox.setFixedSize(500,200);
    }

	// function for blending colours
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	/*******************************************
	Back face culling
	********************************************/
	glPolygonMode(GL_FRONT, GL_FILL);
    glFrontFace(GL_CCW);
	glCullFace(GL_BACK); // cull back faces - do not display them
	glEnable(GL_CULL_FACE); // enable back face culling

	/*******************************************
	Hidden Surface Removal
	********************************************/
    glDepthFunc(GL_LEQUAL);
    glClearDepth(1.0f);                         // Depth Buffer Setup
    glEnable(GL_DEPTH_TEST);

	/*******************************************
	 * Add a light so we can use lighting
	 * and have blending / alpha?
	 *******************************************/



    GLfloat matColour[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    GLfloat matSpecular[] = { 0.5, 0.5, 0.5, 0.5 };
    GLfloat matShininess[] = { 20.0 };

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, matColour);
    glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, matShininess);


    GLfloat lightAmbient[]=  { 0.5f, 0.5f, 0.5f, 1.0f };    // Ambient Light Values
    GLfloat lightDiffuse[]=  { 0.25f, 0.25f, 0.25f, 1.0f };    // Diffuse Light Values
    GLfloat lightPosition[]= { 0.0f, 0.0f, 5.0f, 1.0f };    // Light Position

    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);             // Setup The Ambient Light
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);             // Setup The Diffuse Light
	glLightfv(GL_LIGHT0, GL_POSITION,lightPosition);            // Position The Light

}

void MyQtGLWindow::draw()
{
	drawPrincipleAxis();
}

void MyQtGLWindow::resizeGL(int width, int height)
{
	// Initialise OpenGL properties
    // Set 2D orthographic projection

    glViewport( 0, 0, (GLint)width, (GLint)height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glFrustum( -1.0, 1.0, -1.0, 1.0, nearPlane, farPlane );

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

}

void MyQtGLWindow::paintGL()
{
    glClearColor(0.8f, 0.8f, 1.0f, 0.0f);			// specify background color to lovely light blue

    // 1. Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// save transformations prior to rendering scene
	glPushMatrix();

		// position camera - viewing transformations
		glTranslatef(0.0, 0.0, -cameraR);
		glRotatef(cameraXZ, 1.0, 0.0, 0.0);
		glRotatef(cameraY, 0.0, 1.0, 0.0);

		draw();

	glPopMatrix();

    glFlush();
}

void MyQtGLWindow::mousePressEvent(QMouseEvent *event)
{
	lastPos = event->pos();
}

void MyQtGLWindow::mouseMoveEvent(QMouseEvent *event)
{
	GLfloat dx = GLfloat(event->x() - lastPos.x());
	GLfloat dy = GLfloat(event->y() - lastPos.y());

	if(event->buttons() & Qt::LeftButton)
	{
		dispCameraY(dx);
		dispCameraXZ(dy);
  //      updateGL(); //parent method not overloaded by this class
	}
	lastPos = event->pos();
}

void MyQtGLWindow::wheelEvent(QWheelEvent *event)
{
    int clicks = event->delta();
    dispCameraR(clicks*1.0f);
}

void MyQtGLWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Plus || event->key() == Qt::Key_Q)
    {
        dispCameraR(1.0f);
    }

    if(event->key() == Qt::Key_Minus || event->key() ==Qt::Key_A)
    {
       dispCameraR(-1.0f);
    }

}

void MyQtGLWindow::dispCameraXZ(float f)
{
	cameraXZ += f;
	updateGL(); //parent method not overloaded by this class
}

void MyQtGLWindow::dispCameraY(float f)
{
	cameraY += f;
	updateGL(); //parent method not overloaded by this class
}

void MyQtGLWindow::dispCameraR(float f)
{
	cameraR += f;
	updateGL(); //parent method not overloaded by this class
}

void MyQtGLWindow::setCameraR(float f)
{
	cameraR = f;
	updateGL(); //parent method not overloaded by this class
}

void MyQtGLWindow::drawPrincipleAxis()
{
	// Draw principle axes
	glPushMatrix();
		glScalef(0.65f, 1.0, 1.0);

		glColor3f(1.0, 0.0, 0.0);
		glBegin(GL_LINES);
			glVertex3f(0.0, 0.0, 0.0);
			glVertex3f(1.0, 0.0, 0.0);

			glVertex3f(1.025f, 0.025f, 0.0);
			glVertex3f(1.075f, 0.075f, 0.0);

			glVertex3f(1.025f, 0.075f, 0.0);
			glVertex3f(1.075f, 0.025f, 0.0);
		glEnd();
	glPopMatrix();


	glPushMatrix();
		glScalef(1.0, 0.65f, 1.0);
		glColor3f(0.0, 1.0, 0.0);
		glBegin(GL_LINES);
			glVertex3f(0.0, 0.0, 0.0);
			glVertex3f(0.0, 1.0, 0.0);

			glVertex3f(-0.075f, 1.075f, 0.0);
			glVertex3f(-0.05f, 1.05f, 0.0);

			glVertex3f(-0.025f, 1.075f, 0.0);
			glVertex3f(-0.075f, 1.025f, 0.0);
		glEnd();
	glPopMatrix();


	glPushMatrix();
		glScalef(1.0, 1.0, 0.65f);
		glColor3f(0.0, 0.0, 1.0);
		glBegin(GL_LINES);
			glVertex3f(0.0, 0.0, 0.0);
			glVertex3f(0.0, 0.0, 1.0);

			glVertex3f(0.025f, 0.075f, 1.0);
			glVertex3f(0.075f, 0.075f, 1.0);

			glVertex3f(0.075f, 0.075f, 1.0);
			glVertex3f(0.025f, 0.025f, 1.0);

			glVertex3f(0.025f, 0.025f, 1.0);
			glVertex3f(0.075f, 0.025f, 1.0);
		glEnd();
	glPopMatrix();
}
