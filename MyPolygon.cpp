#include "MyPolygon.h"
#include "Vector3f.h"
#include "MyPolygon.h"

#include <iostream>

using std::cout;
using std::endl;

MyPolygon::MyPolygon(void)
{
	indexCount = 0;
}

MyPolygon::~MyPolygon(void)
{
	while(!vertIndicies.empty())
	{
		vertIndicies.pop_back();	
	}

	while(!normIndicies.empty())
	{
		normIndicies.pop_back();	
	}

    while(!textIndicies.empty())
    {
        textIndicies.pop_back();
    }
}

MyPolygon::MyPolygon(int n)
{
	indexCount = n;
}

int MyPolygon::getIndexCount()
{
	return indexCount;
}

void MyPolygon::setIndexCount(int i)
{
	indexCount = i;
}

void MyPolygon::addVertIndex(int i)
{
	vertIndicies.push_back(i);
}

void MyPolygon::addNormIndex(int i)
{
	normIndicies.push_back(i);
}

void MyPolygon::addTextIndex(int i)
{
    textIndicies.push_back(i);
}

int MyPolygon::getVertIndexN(int i)
{
	return vertIndicies.at(i);
}

int MyPolygon::getNormIndexN(int i)
{
	return normIndicies.at(i);
}

int MyPolygon::getTextIndexN(int i)
{
    return textIndicies.at(i);
}

void MyPolygon::printVertIndex()
{
    cout << "-------- Start print vertex Indexes -------------" << endl;
    for(int i=0; i< vertIndicies.size(); i++)
    {
        cout << "Vertex Index " << i << " : " << vertIndicies.at(i) << endl;
    }
    cout << "-------- End print vertex Indexes ---------------\n" << endl;
}

void MyPolygon::printNormIndex()
{
    cout << "-------- Start print normal Indexes -------------\n" << endl;
    for(int i=0; i< normIndicies.size(); i++)
    {
        cout << "Normal Index " << i << " : " <<normIndicies.at(i) << endl;
    }
    cout <<  "-------- End print normal Indexes ---------------\n" << endl;
}

void MyPolygon::printTextIndex()
{
    cout << "-------- Start print texture Indexes -------------\n" << endl;
    for(int i=0; i< textIndicies.size(); i++)
    {
        cout << "Texture Index " << i << " : " <<textIndicies.at(i) << endl;
    }
    cout <<  "-------- End print texture Indexes ---------------\n" << endl;
}
