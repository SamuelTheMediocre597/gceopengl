/*
 * MyQtGLWindow.h
 *
 *  Created on: 16-May-2009
 *      Author: tigger
 */

#ifndef MY_QTGL_WINDOW_H_
#define MY_QTGL_WINDOW_H_

#include <qgl.h>
#include <QtGui>

class MyQtGLWindow : public QGLWidget
{
	Q_OBJECT

private:

	GLfloat cameraXZ;
	GLfloat cameraR;
	GLfloat cameraY;

	GLfloat vpWidth;
	GLfloat vpHeight;
	GLfloat nearPlane;
	GLfloat farPlane;

	QPoint lastPos;

protected:
	virtual void draw();
	void initalizeGL();
	void resizeGL(int width, int height);
	void paintGL();
	void mousePressEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void keyPressEvent(QKeyEvent *event);


public:
    MyQtGLWindow(QWidget *parent=0);
    virtual ~MyQtGLWindow();

    virtual void init();

    void dispCameraXZ(float f);
	void dispCameraR(float f);
	void dispCameraY(float f);
	void setCameraR(float f);

	void drawPrincipleAxis();

};

#endif /* MyQtGLWindow_H_ */
