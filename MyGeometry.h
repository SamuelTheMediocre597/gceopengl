#pragma once
#include "Object3D.h"

#include <vector>
using std::vector;

class Vector3f;
class MyPolygon;

class MyGeometry : public Object3D
{

private:
	int vertexCount; // in collada these will be the same as it has a normal per vertex
	int normalCount; // this is not always the case. 

	vector<Vector3f*> verts; //verticies
	vector<Vector3f*> norms; //normals

	vector<MyPolygon*> polys; //polygons - specify the verts and norms of each polygon
							  //might be triangles, quads etc. 
    Vector3f scale;

public:
	MyGeometry(void);
	~MyGeometry(void);

	int getVertexCount();
	int getNormalCount();

	void addVertex(Vector3f*);
	void addNormal(Vector3f*);

	void removeVertex();
	void removeNormal();

	void addVertNormPair(Vector3f* v, Vector3f* n);
	void removeVertexNormPair();

	Vector3f* getVertexN(int);
	Vector3f* getNormalN(int);

	MyPolygon* getPolygonN(int);
	void addPolygon(MyPolygon *p);

	void draw(void); //this overrides the parent method, which is virtual. 

    //Badly need this some models are massive
    void setScale(float x, float y, float z);

    //Debugging methods.
    void printVerts();
    void printNormals();
    void printPolyIndexes();
};
