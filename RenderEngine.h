#ifndef RENDERENGINE_H
#define RENDERENGINE_H

#include "MyQtGLWindow.h"

class Object3D;

#include <vector>
using std::vector;



class RenderEngine : public MyQtGLWindow
{
private:

    //List of shapes to be drawn, the standard library template vector is used
    //this is a dynamic array making handeling of an arbritrarily long draw list simple.
    vector<Object3D*>	drawList;
    //Note: A scene graph of some sort would probably be used in game to identify objects for
    //rendering given a current camera position.

    //flag to toggle axis
    bool drawAxis;

public:
    RenderEngine(QWidget* parent = 0);
    virtual ~RenderEngine();

    void init();
    void draw();

    void setDrawList(vector<Object3D*>);
    vector<Object3D*> getDrawList(void);

    void add(Object3D *obj); //basically does a push
    void remove();			// and a pop

    void axisOn();
    void axisOff();



};

#endif // RENDERENGINE_H
