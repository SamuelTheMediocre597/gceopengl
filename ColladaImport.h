#ifndef _COLLADA_IMPORT_H_
#define _COLLADA_IMPORT_H_


//Cant forward declare the collada stuff, far to complicated.

#include <dae.h>
#include <dae/daeDomTypes.h>
#include <dom/domCOLLADA.h>
#include <dom/domProfile_CG.h>
#include <dom/domConstants.h>
#include <dom/domMesh.h>
#include <dae/daeDomTypes.h>
#include <dae/daeURI.h>
#include <dae/daeUtils.h>

using ColladaDOM141::domListOfUInts;
using ColladaDOM141::domMesh;
using ColladaDOM141::domTriangles;
using ColladaDOM141::domGeometry;
using ColladaDOM141::domInputLocalOffsetRef;
using ColladaDOM141::domFloat_array;
using ColladaDOM141::domSource;

//Use forward decleration for my objects
class MyGeometry;

class ColladaImport
{
protected:
		DAE *dae;
		daeElement* root;

		MyGeometry* local;
        std::vector<MyGeometry*> list;

		bool fileLoadedFlag;
		void printChildren(char* idMessage, daeElement* root);
		void extractMyGeometry();
        void getTriangles(domMesh* meshElement, MyGeometry* local);
        void getMetadata(domTriangles *t, int *vOffset, int *nOffset, int *tOffset, char** vertexArrayID, char** normalArrayID, char** textureArrayID);
        void processIndexes(MyGeometry* geom, domListOfUInts P, int indexCount, int pStep, int nOffset, int vOffset, int tOffset);
        void processVertexes(MyGeometry* geom, domMesh* meshElement, int sourceArrayCount, char* vertexArrayID, char* positionArrayID, char* normalArrayID);
		
public:
	ColladaImport(void);
	~ColladaImport(void);
	void loadColladaFile(char *URL);
    std::vector<MyGeometry*> getMyGeom(void);
		
};

#endif
